# EvolveSMS Theme - Goergia Tech


This is a Georgia Tech theme for the EvolveSMS Android Application. It's purpose is for the author's personal learning experience and fun.

  * It does not use the EvolveSMS Fancy Headers.
  * It uses the official colors and logos as set by the GT Licensing and Trademark Department
  * Why a Georgia Tech theme?  [Because this](http://www.youtube.com/watch?v=U_U4BBjC8tk "You Can Do That!")
 
---
Thank you to  mattg12 for his artwork rendition of the Buzz logo.  You can find the original here: http://mattgt12.blogspot.com/2012_01_01_archive.html

---

Please contact me if you have any questions

Email: gwesterberg@gmail.com

---

## License

    Copyright 2014 Glen Westerberg

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.